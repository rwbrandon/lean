# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.19)
# Database: lean_test_db
# Generation Time: 2017-12-04 16:22:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) unsigned NOT NULL,
  `title` varchar(40) NOT NULL DEFAULT '',
  `location` varchar(100) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `time` time NOT NULL,
  `refreshments` tinyint(1) DEFAULT NULL,
  `description` text NOT NULL,
  `attendance` int(11) DEFAULT NULL,
  `category` varchar(20) NOT NULL DEFAULT '',
  `posted` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Hosts` (`organization_id`),
  CONSTRAINT `Hosts` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `organization_id`, `title`, `location`, `date`, `time`, `refreshments`, `description`, `attendance`, `category`, `posted`)
VALUES
	(4,4,'Final exam study','Strozier Library','2017-12-10','18:00:00',1,'Come study and have fun with us!!!!',NULL,'Educational','2017-12-02 20:38:07'),
	(5,3,'Annual cookout','4903 SW D Ave','2017-12-11','15:00:00',1,'Come eat and get FAT!!!!',NULL,'Recreational','2017-12-02 20:38:07'),
	(8,1,'Thursday night','620 W Tennessee St Tallahassee, Florida','2017-12-09','21:00:00',1,'Come get lit!!!!',NULL,'Social','2017-12-02 20:47:39');

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table organization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `name` varchar(40) NOT NULL,
  `location` varchar(100) NOT NULL DEFAULT '',
  `telephone` varchar(20) NOT NULL DEFAULT '',
  `website` varchar(40) DEFAULT '',
  `registered` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;

INSERT INTO `organization` (`id`, `email`, `username`, `password`, `name`, `location`, `telephone`, `website`, `registered`)
VALUES
	(1,'bullstally@gmail.com','bullwinkles','$2b$12$DKjdam/QiR/AlGIsAjIKQuCbi1uP7UPGfZV1OFW2eK1XANVEdIfba','Bullwinkle\'s Saloon','620 W Tennessee St Tallahassee, Florida','(850) 224-0651','http://www.Bullwinklessaloon.com','2017-11-27 21:27:55'),
	(2,'a@a.com','a','$2b$12$H3zTxwVyFz35W4uorkvE/.msh4HIgqyl0q6jxl3tVnIz2SrNIi/o6','a','a','a','a','2017-12-01 13:42:50'),
	(3,'freemason@lean.com','freemason','$2b$12$IAeFdPYuASEvieBWcsf/oehKjVw90Hqe0i5jrouZ//w1kjEvY2L.2','Free Mason Pub','4903 SW D Ave','(123)456-7821','','2017-12-02 20:28:58'),
	(4,'chipiomega@lean.com','chipiomega','$2b$12$C/RYJU0Ye0nrmAPJYnzLeuOeJrfZsHasqrJxL46PCwiolE/SWDdt2','Chi Pi Omega Kappa','908 Freemason Ave','(890)365-0000','','2017-12-02 20:28:58');

/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned DEFAULT NULL,
  `org_id` int(11) unsigned DEFAULT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(60) NOT NULL DEFAULT '',
  `fullname` varchar(60) NOT NULL DEFAULT '',
  `dob` date NOT NULL,
  `gender` char(1) DEFAULT NULL,
  `registered` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_telephone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `Attendee` (`event_id`),
  KEY `Member` (`org_id`),
  CONSTRAINT `Attendee` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `Member` FOREIGN KEY (`org_id`) REFERENCES `organization` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `event_id`, `org_id`, `email`, `username`, `password`, `fullname`, `dob`, `gender`, `registered`, `user_telephone`)
VALUES
	(11,NULL,NULL,'peter@lean.com','Peter1','$2b$12$tfE1YnIBGuD9uJclMIJHXO62DzfiK2ygFFQ9PH671hFvd/9eChQAu','Peter Paul','1995-05-23',NULL,'2017-12-02 20:18:07',NULL),
	(12,NULL,NULL,'john@lean.com','John1','$2b$12$yR0iAk8Uxdfw5xtUM.rl0eN17BmqhynZT0GcJcF/Pkf2z8yYyIad2','John Smith','1995-05-25',NULL,'2017-12-02 20:18:07',NULL),
	(13,NULL,NULL,'sarah@lean.com','Sarah1','$2b$12$4nW1ceJdwGo2Chkm7XHDXOQRIz5RG488hP4cVYFkZ9TDhJKXgw6OG','Sarah Bill','1995-04-25',NULL,'2017-12-02 20:18:07',NULL),
	(14,NULL,NULL,'smith@lean.com','Smith1','2b$12$4fwpzQUUqRo806vyeglFNuieGjylo7MEqUvVpD41UOQ5b79kRSx46','Smith Harper','1996-04-25',NULL,'2017-12-02 20:18:07',NULL),
	(15,NULL,NULL,'brandon@mail.com','brandon','$2b$12$uIvD2oRr1mi/WzXL8VRLMufI.gM7Xlvj5NUiyPP5T/N8BqISJd8tS','Brandon Rivera','1995-02-09','M','2017-12-04 11:15:10',NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
