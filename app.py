from flask import Flask, render_template, jsonify, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SelectField
from wtforms.validators import InputRequired, EqualTo
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user, confirm_login
import bcrypt
import os
import datetime
import logging
from logging.handlers import RotatingFileHandler

class LoginForm(FlaskForm):
	username = StringField('username', validators=[InputRequired()], render_kw={"placeholder": "Username"})
	password = PasswordField('password', validators=[InputRequired()], render_kw={"placeholder": "Password"})

class UserRegisterForm(FlaskForm):
	email = StringField('email', validators=[InputRequired()], render_kw={"placeholder": "Email"})
	username = StringField('username', validators=[InputRequired()], render_kw={"placeholder": "Username"})
	fullname = StringField('name', validators=[InputRequired()], render_kw={"placeholder": "Full name"})
	month = SelectField('month', validators=[InputRequired()])
	day = SelectField('day', validators=[InputRequired()])
	year = SelectField('year', validators=[InputRequired()])
	gender = SelectField('gender', validators=[InputRequired()])
	password = PasswordField('password', validators=[InputRequired(), EqualTo('confirm', message='Passwords must match')], render_kw={"placeholder": "Password"})
	confirm = PasswordField('Confirm password', render_kw={"placeholder": "Confirm password"})

class OrgRegisterForm(FlaskForm):
	email = StringField('email', validators=[InputRequired()], render_kw={"placeholder": "Email"})
	username = StringField('username', validators=[InputRequired()], render_kw={"placeholder": "Username"})
	name = StringField('name', validators=[InputRequired()], render_kw={"placeholder": "Name"})
	password = PasswordField('password', validators=[InputRequired(), EqualTo('confirm', message='Passwords must match')], render_kw={"placeholder": "Password"})
	confirm = PasswordField('Confirm password', render_kw={"placeholder": "Confirm password"})
	location = StringField('location', validators=[InputRequired()], render_kw={"placeholder": "Location"})
	telephone = StringField('telephone', validators=[InputRequired()], render_kw={"placeholder": "Telephone"})
	website = StringField('website', validators=[InputRequired()], render_kw={"placeholder": "Website"})

app = Flask(__name__)

# Configurations
app.config['SECRET_KEY'] = os.urandom(12)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://user:password@localhost/lean_test_db'

database = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)

class User(UserMixin, database.Model):
	id = database.Column(database.Integer, primary_key=True)
	email = database.Column(database.String(50), unique=True)
	username = database.Column(database.String(30), unique=True)
	password = database.Column(database.String(60))
	fullname = database.Column(database.String(60))
	dob = database.Column(database.Date())
	registered = database.Column(database.DateTime())
	gender = database.Column(database.CHAR(1))

	def __init__(self, email, username, password, fullname, dob, gender, registered):
		self.email = email
		self.username = username
		self.password = password
		self.fullname = fullname
		self.dob = dob
		self.gender = gender
		self.registered = registered

	def verify_password(self, password):
		return bcrypt.checkpw(password.encode(), self.password.encode())

class Organization(UserMixin, database.Model):
	id = database.Column(database.Integer, primary_key=True)
	email = database.Column(database.String(50), unique=True)
	username = database.Column(database.String(30), unique=True)
	password = database.Column(database.String(60))
	name = database.Column(database.String(60))
	location = database.Column(database.String(100))
	telephone = database.Column(database.String(20))
	website = database.Column(database.String(20))
	registered = database.Column(database.DateTime())

	def __init__(self, email, username, password, name, location, telephone, website, registered):
		self.email = email
		self.username = username
		self.password = password
		self.name = name
		self.location = location
		self.telephone = telephone
		self.website = website
		self.registered = registered

	def verify_password(self, password):
		return bcrypt.checkpw(password.encode(), self.password.encode())

@login_manager.user_loader
def load_user(user_id):
	if User.query.get(int(user_id)) == None:
		return Organization.query.get(int(user_id))
	return User.query.get(int(user_id))

@app.route('/', methods=['GET', 'POST'])
def index():
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		if user is not None and user.verify_password(form.password.data):
			login_user(user)
			return redirect(url_for('home'))
		org = Organization.query.filter_by(username=form.username.data).first()
		if org is not None and org.verify_password(form.password.data):
			login_user(org)
			return redirect(url_for('home'))

	return render_template('index.html', form=form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))

@login_manager.unauthorized_handler
def unauthorized():
	return render_template('redirect.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
	userForm = UserRegisterForm()
	orgForm = OrgRegisterForm()

	userForm.month.choices = [
		('', 'Month'),
		('1', 'January'),
		('2', 'February'),
		('3', 'March'),
		('4', 'April'),
		('5', 'May'),
		('6', 'June'),
		('7', 'July'),
		('8', 'August'),
		('9', 'September'),
		('10', 'October'),
		('11', 'November'),
		('12', 'December'),
	]
	userForm.day.choices = []
	userForm.day.choices.append(('', 'Day'))
	for i in range(1, 32):
		userForm.day.choices.append((str(i), i))
	userForm.year.choices = []
	userForm.year.choices.append(('', 'Year'))
	current_year = datetime.datetime.now().year
	for i in range(1900, current_year+1):
		userForm.year.choices.append((str(i), i))
	userForm.gender.choices = [
		('', 'Gender'),
		('M', 'Male'),
		('F', 'Female')
	]
	if userForm.validate_on_submit():
		email = userForm.email.data
		username = userForm.username.data
		password = userForm.password.data
		hashed_password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
		fullname = userForm.fullname.data
		month = int(userForm.month.data)
		day = int(userForm.day.data)
		year = int(userForm.year.data)
		date = datetime.datetime(year, month, day, 0, 0)
		date_formatted = date.strftime('%Y-%m-%d')
		gender = userForm.gender.data
		registered = datetime.datetime.now()

		user = User(email, username, hashed_password, fullname, date_formatted, gender, registered)
		database.session.add(user)
		database.session.commit()
		return redirect(url_for('index'))
	elif orgForm.validate_on_submit():
		email = orgForm.email.data
		username = orgForm.username.data
		password = orgForm.password.data
		hashed_password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
		name = orgForm.name.data
		location = orgForm.location.data
		telephone = orgForm.telephone.data
		website = orgForm.website.data
		registered = datetime.datetime.now()

		org = Organization(email, username, hashed_password, name, location, telephone, website, registered)
		database.session.add(org)
		database.session.commit()
		return redirect(url_for('index'))

	return render_template('register.html', userForm=userForm, orgForm=orgForm, year=current_year)

@app.route('/home')
@login_required
def home():
	confirm_login()
	# return 'user: ' + current_user.username
	return render_template('home.html', user=current_user)

@app.route('/profile')
@login_required
def profile():
	return render_template('profile.html', user=current_user)

if __name__ == '__main__':
	handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
	handler.setLevel(logging.INFO)
	app.logger.addHandler(handler)
	app.run(debug=True)